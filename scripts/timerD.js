var myapp = angular.module('app', []);

myapp.controller("timerController", function($scope, $http) {
	
		$scope.title="Timer";
		
		function plus(){
			$scope.mins=$scope.mins+15;
			if($scope.mins>60)
			{
				$scope.hours=$scope.hours+1;
				$scope.mins=$scope.mins-60;
			}	
			
			if($scope.hours>23)
			{
				$scope.hours=0;
			}		
		}
		
		function subtract(){
			$scope.mins=$scope.mins-15;
			if($scope.mins<0)
			{
				$scope.hours=$scope.hours-1;
				$scope.mins=$scope.mins+60;
			}	
			
			if($scope.hours<0)
			{
				$scope.hours=23;
			}					
		}
		
		 $scope.add=function(){
		console.log("add");
		 console.log($scope.hours);
			plus();		
		 }
		  
		$scope.minus= function (index,item) {
			subtract();			
		};

		$scope.check=function(){
			var currtime = new Date();
			var currHr=currtime.getHours() ;
			var currMin=currtime.getMinutes(); 
			console.log("check"+ currHr +":"+ currMin);
			
			
			var inpHr=$scope.hours;
			var inpMin=$scope.mins;
			
			console.log(inpHr);
	
			if(currHr==inpHr && currMin==inpMin){
				alert("Success");
			}
			else
				alert("failure");
		}

});


myapp.directive('timer', function() {
	
	return{
		restrict:'E',
		templateUrl:'modules/timerTemplate.html',
		scope:false,
				
		// link:function(scope, element, attrs){	
			// scope.title="hello";		
		// },
		
		// controller:['$scope', function ($scope) {
				 
		// function plus(){
			// $scope.mins=$scope.mins+15;
			// if($scope.mins>60)
			// {
				// $scope.hours=$scope.hours+1;
				// $scope.mins=$scope.mins-60;
			// }	
			
			// if($scope.hours>23)
			// {
				// $scope.hours=0;
			// }		
		// }
		
		// function subtract(){
			// $scope.mins=$scope.mins-15;
			// if($scope.mins<0)
			// {
				// $scope.hours=$scope.hours-1;
				// $scope.mins=$scope.mins+60;
			// }	
			
			// if($scope.hours<0)
			// {
				// $scope.hours=23;
			// }					
		// }
		
		 // $scope.add=function(){
		// console.log("add");
		 // console.log($scope.hours);
			// plus();		
		 // }
		  
		// $scope.minus= function (index,item) {
			// subtract();			
		// };	
			
      // }]

		
	};
});

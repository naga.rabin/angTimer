(function(){
  angular.module("app", [])
  .directive("timer", function(){
    return {
      restrict : "E",
	  templateUrl:'modules/timerTemplate.html',
      scope : { 
		hours:"=myhours",
		mins:"=mymins",
      },
      controller:['$scope', function ($scope){
	  
		function plus(){
			$scope.mins=$scope.mins+15;			
			if($scope.mins>60)
			{
				$scope.hours=$scope.hours+1;
				$scope.mins=$scope.mins-60;
			}	
			
			if($scope.hours>23)
			{
				$scope.hours=0;
			}		
		}
		
		function subtract(){
			$scope.mins=$scope.mins-15;
			if($scope.mins<0)
			{
				$scope.hours=$scope.hours-1;
				$scope.mins=$scope.mins+60;
			}	
			
			if($scope.hours<0)
			{
				$scope.hours=23;
			}					
		}
		
		 $scope.add=function(){
			plus();		
		 }
		  
		$scope.minus= function (index,item) {
			subtract();			
		}	
        
      }]
    }
  })
  .controller("timerController", function($scope){			
		
		$scope.check=function(){
			var currtime = new Date();
			var currHr=currtime.getHours() ;
			var currMin=currtime.getMinutes(); 
			console.log("check"+ currHr +":"+ currMin);
			
			var inpHr=$scope.hours;
			var inpMin=$scope.mins;
	
			if(currHr==inpHr && currMin==inpMin){
				alert("Success");
			}
			else
				alert("failure");
		}				
  });
})();

